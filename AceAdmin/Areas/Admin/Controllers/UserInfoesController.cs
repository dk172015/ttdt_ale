﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AceAdmin.Common;
using AceAdmin.Models;
using AceAdmin.Repository;
using AceModel.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace AceAdmin.Areas.Admin.Controllers
{
    //[Authorize]
    [Roles(PublicConstant.ROLE_ADMIN)]
    public class UserInfoesController : Controller
    {
        private AceAdminEntities db = new AceAdminEntities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public UserInfoesController()
        {
        }
        public UserInfoesController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationSignInManager _signInManager;
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        // GET: Admin/UserInfoes
        public ActionResult Index()
        {
            return View(db.UserInfoes.ToList());
        }

        // GET: Admin/UserInfoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInfo userInfo = db.UserInfoes.Find(id);
            if (userInfo == null)
            {
                return HttpNotFound();
            }
            return View(userInfo);
        }

        public ActionResult CreateUser()
        {
            return View();
        }
        [HttpPost]

        public async Task<ActionResult> CreateUser(AddUserViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser
                    {
                        UserName = model.UserName,
                        TwoFactorEnabled = true,
                        Email = Request.Form["email"],
                    };
                    //Models.sys_users userInfo = new Models.sys_users();
                    AceAdmin.Models.UserInfoes userInfo = new AceAdmin.Models.UserInfoes();
                    userInfo.Address = Request.Form["address"];
                    userInfo.Created_At = DateTime.Now;
                    //userInfo.FullName = Request.Form["hoten"];
                    userInfo.Email = Request.Form["email"];
                    userInfo.Phone = Request.Form["Phone"];
                    userInfo.ApplicationUserID = user.Id;
                    userInfo.UserName = model.UserName;

                    string userId = User.Identity.GetUserId();
                    userInfo.Status = 1;
                    user.UsersInfo = userInfo;
                    var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                        UserManager.AddToRole(user.Id, PublicConstant.ROLE_USERS.ToString());
                        TempData["message"] = "Tạo tào khoản thành công!";
                        return RedirectToAction("Index", "UserInfoes");
                    }
                    AddErrors(result);
                }
                else
                {
                    var errors = ModelState.Values.SelectMany(v => v.Errors);
                }

            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi tạo tài khoản";
                string mss = ex.Message;
                throw;
            }
            return View();
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }


        public JsonResult ListCarType(string userId = "")
        {
            UserCateModel listUserCarViewModel = new UserCateModel();
            List<Category> lstcardtype = _unitOfWork.GetRepositoryInstance<Category>().GetAllRecords().ToList();
            listUserCarViewModel.categories = lstcardtype;
            if (userId != "")
            {
                AceModel.Entity.UserCate cardtypeChecked = _unitOfWork.GetRepositoryInstance<UserCate>().GetListByParameter(x => x.UserId == userId).SingleOrDefault();
                ViewBag.lstcardtypeChecked = cardtypeChecked;
                listUserCarViewModel.userinfoes = cardtypeChecked;
            }


            return Json(listUserCarViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListCate(string userId = "")
        {
            UserCateModel listUserCarViewModel = new UserCateModel();
            List<Category> lstcardtype = _unitOfWork.GetRepositoryInstance<Category>().GetAllRecords().ToList();
            listUserCarViewModel.categories = lstcardtype;
            if (userId != "")
            {
                AceModel.Entity.UserCate cardtypeChecked = _unitOfWork.GetRepositoryInstance<UserCate>().GetListByParameter(x => x.UserId == userId).SingleOrDefault();
                ViewBag.lstcardtypeChecked = cardtypeChecked;
                listUserCarViewModel.userinfoes = cardtypeChecked;
            }


            return View(listUserCarViewModel);
        }


        // GET: Admin/UserInfoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInfo userInfo = db.UserInfoes.Find(id);
            if (userInfo == null)
            {
                return HttpNotFound();
            }
            return View(userInfo);
        }

        // POST: Admin/UserInfoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,Phone,Address,Created_At,Updated_At,Status,ApplicationUserID")] UserInfo userInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userInfo);
        }

        // GET: Admin/UserInfoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInfo userInfo = db.UserInfoes.Find(id);
            if (userInfo == null)
            {
                return HttpNotFound();
            }
            return View(userInfo);
        }

        // POST: Admin/UserInfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserInfo userInfo = db.UserInfoes.Find(id);
            db.UserInfoes.Remove(userInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public JsonResult XacNhan(string userId, string cardType)
        {
            var usercard = _unitOfWork.GetRepositoryInstance<UserCate>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
            if (usercard != null)
            {
                usercard.CateId = cardType;
                _unitOfWork.GetRepositoryInstance<UserCate>().Update(usercard);
            }
            else
            {
                usercard = new UserCate();
                usercard.UserId = userId;
                usercard.CateId = cardType;
                _unitOfWork.GetRepositoryInstance<UserCate>().Add(usercard);
            }
            TempData["messages"] = "Cấu hình thành công!";
            _unitOfWork.SaveChanges();
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
