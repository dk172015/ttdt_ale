﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AceAdmin.Areas.Admin.Models;
using AceAdmin.Common;
using AceModel.Entity;

namespace AceAdmin.Areas.Admin.Controllers
{
    public class ArticlesController : Controller
    {
        private AceAdminEntities db = new AceAdminEntities();

        // GET: Article
        public ActionResult Index()
        {
            var Article = db.Articles.Include(a => a.Category).OrderByDescending(o => o.Id);
            return View(Article.ToList());
        }

        // GET: Article/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article Article = db.Articles.Find(id);
            if (Article == null)
            {
                return HttpNotFound();
            }
            return View(Article);
        }

        // GET: Article/Create
        public ActionResult Create()
        {
            ViewBag.Status = Utitlitys.StatusToList();

            List<Category> listITem = new List<Category>();
            List<Category> listCateParent;
            List<Category> listCateChild;
            Category dopDowItem;
            listCateParent = db.Categories.Where(o => o.ParentId == null).OrderBy(o => o.Id).ToList();

            foreach (Category item in listCateParent)
            {
                dopDowItem = new Category();
                listCateChild = db.Categories.Where(o => o.ParentId == item.Id).ToList();
                if (listCateChild.Count > 0)
                {
                    listITem.Add(item);
                    foreach (Category itemCchild in listCateChild)
                    {
                        itemCchild.Name = "---- " + itemCchild.Name;
                        listITem.Add(itemCchild);
                    }
                }
                else
                {
                    listITem.Add(item);
                }
            }

            ViewBag.CategoryId = new SelectList(listITem, "Id", "Name");
            return View();
        }

        // POST: Article/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(ArticleViewModel Article, IEnumerable<HttpPostedFileBase> files)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var fileName = string.Empty;
                    if (files != null)
                    {
                        foreach (var file in files)
                        {
                            if (file != null && file.ContentLength > 0)
                            {
                                //kiem tra file extension lan nua
                                List<string> excelExtension = new List<string>();
                                excelExtension.Add(".png");
                                excelExtension.Add(".jpg");
                                excelExtension.Add(".gif");
                                var extension = Path.GetExtension(file.FileName);
                                if (!excelExtension.Contains(extension.ToLower()))
                                {
                                    return Json(new { success = 0, message = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png" });
                                }
                                //kiem tra dung luong file
                                var fileSize = file.ContentLength;
                                var fileMB = (fileSize / 1024f) / 1024f;
                                if (fileMB > 5)
                                {
                                    return Json(new { success = 0, message = "Dung lượng ảnh không được lớn hơn 5MB" });
                                }
                                // luu ra dia
                                fileName = Path.GetFileNameWithoutExtension(file.FileName);
                                //fileName = fileName + "_" + user.UserID + extension;
                                fileName = fileName + "-" + DateTime.Now.ToString("dd-MM-yy-hh-ss") + extension;
                                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/TinTuc"), fileName);
                                file.SaveAs(physicalPath);
                                Article.articleInfo.ThumbURL = fileName;
                            }
                        }
                    }
                    Article.articleInfo.Summary = Article.Summary;
                    Article.articleInfo.Description = Article.Description;
                    Article.articleInfo.Summary_EN = Article.Summary_EN;
                    Article.articleInfo.Description_EN = Article.Description_EN;
                    Article.articleInfo.CreateAt = DateTime.Now;
                    Article.articleInfo.CreateBy = User.Identity.Name;
                    Article.articleInfo.Category = db.Categories.Find(Article.articleInfo.CategoryId);
                    db.Articles.Add(Article.articleInfo);
                    db.SaveChanges();
                    TempData["info"] = "Thêm bài viết thành công";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Lỗi thêm bài viết: " + ex.Message;
                }
            }

            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", Article.articleInfo.CategoryId);
            return View(Article);
        }

        // GET: Article/Edit/5
        public ActionResult Edit(long? id)
        {
            ViewBag.Status = Utitlitys.StatusToList();
            //ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article Article = db.Articles.Find(id);
            if (Article == null)
            {
                return HttpNotFound();
            }

            List<Category> listITem = new List<Category>();
            List<Category> listCateParent;
            List<Category> listCateChild;
            Category dopDowItem;
            listCateParent = db.Categories.Where(o => o.ParentId == null).OrderBy(o => o.Id).ToList();

            foreach (Category item in listCateParent)
            {
                dopDowItem = new Category();
                listCateChild = db.Categories.Where(o => o.ParentId == item.Id).ToList();
                if (listCateChild.Count > 0)
                {
                    listITem.Add(item);
                    foreach (Category itemCchild in listCateChild)
                    {
                        itemCchild.Name = "---- " + itemCchild.Name;
                        listITem.Add(itemCchild);
                    }
                }
                else
                {
                    listITem.Add(item);
                }
            }

            ViewBag.CategoryId = new SelectList(listITem, "Id", "Name", Article.CategoryId);
            ArticleViewModel viewmodel = new ArticleViewModel();
            viewmodel.articleInfo = Article;
            viewmodel.Description = Article.Description;
            viewmodel.Summary = Article.Summary;
            viewmodel.Description_EN = Article.Description_EN;
            viewmodel.Summary_EN = Article.Summary_EN;
            return View(viewmodel);
        }

        // POST: Article/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(ArticleViewModel Article, IEnumerable<HttpPostedFileBase> files)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var old = db.Articles.Find(Article.articleInfo.Id);
                    var fileName = string.Empty;
                    if (files != null)
                    {
                        foreach (var file in files)
                        {
                            if (file != null && file.ContentLength > 0)
                            {
                                try
                                {
                                    //kiem tra file extension lan nua
                                    List<string> excelExtension = new List<string>();
                                    excelExtension.Add(".png");
                                    excelExtension.Add(".jpg");
                                    excelExtension.Add(".gif");
                                    var extension = Path.GetExtension(file.FileName);
                                    if (!excelExtension.Contains(extension.ToLower()))
                                    {
                                        TempData["error"] = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png";
                                        return Json(new { success = 0, message = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png" });
                                    }
                                    //kiem tra dung luong file
                                    var fileSize = file.ContentLength;
                                    var fileMB = (fileSize / 1024f) / 1024f;
                                    if (fileMB > 5)
                                    {
                                        TempData["error"] = "Dung lượng ảnh không được lớn hơn 5MB";
                                        return Json(new { success = 0, message = "Dung lượng ảnh không được lớn hơn 5MB" });
                                    }

                                    if (file.FileName != null && file.FileName.Length > 0)
                                    {
                                        // luu ra dia
                                        fileName = Path.GetFileNameWithoutExtension(file.FileName);
                                        //fileName = fileName + extension;
                                        fileName = fileName + "-" + DateTime.Now.ToString("dd-MM-yy-hh-ss") + extension;
                                    }

                                    var physicalPath = Path.Combine(Server.MapPath("~/Uploads/TinTuc"), fileName);
                                    try
                                    {
                                        file.SaveAs(physicalPath);
                                        System.IO.File.Delete(Server.MapPath("~/Uploads/TinTuc/" + old.ThumbURL));
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    Article.articleInfo.ThumbURL = fileName;
                                    old.ThumbURL = Article.articleInfo.ThumbURL;
                                }
                                catch (Exception ex)
                                {
                                    TempData["error"] = "Lỗi upload ảnh bài viết: " + ex.Message;
                                }
                            }
                        }
                    }
                    Article.articleInfo.Category = db.Categories.Find(Article.articleInfo.CategoryId);
                    old.Category = Article.articleInfo.Category;
                    old.Status = Article.articleInfo.Status;
                    old.Title = Article.articleInfo.Title;
                    old.Title_EN = Article.articleInfo.Title_EN;
                    old.UpdateAt = DateTime.Now;
                    old.UpdateBy = User.Identity.Name;
                    old.Summary = Article.Summary;
                    old.Description = Article.Description;
                    old.Summary_EN = Article.Summary_EN;
                    old.Description_EN = Article.Description_EN;
                    db.Entry(old).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    TempData["info"] = "Cập nhật vài viết thành công ";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Lỗi thêm bài viết: " + ex.Message;
                }
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", Article.articleInfo.CategoryId);
            return View(Article);
        }

        // GET: Article/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article Article = db.Articles.Find(id);
            if (Article == null)
            {
                return HttpNotFound();
            }
            return View(Article);
        }

        // POST: Article/Delete/5
        [System.Web.Mvc.HttpPost, System.Web.Mvc.ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Article Article = db.Articles.Find(id);
            db.Articles.Remove(Article);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
