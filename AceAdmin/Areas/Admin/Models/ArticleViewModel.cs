﻿using AceModel.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AceAdmin.Areas.Admin.Models
{
    public class ArticleViewModel
    {
        public Article articleInfo { get; set; }
        public List<SelectListItem> statusArticles { get; set; }
        [AllowHtml]
        public string Description { get; set; }

        [AllowHtml]
        public string Summary { get; set; }

        [AllowHtml]
        public string Description_EN { get; set; }

        [AllowHtml]
        public string Summary_EN { get; set; }

    }
}