﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AceAdmin.Areas.Admin.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public List<string> Roles { get; set; }
        public string RoleName { get; set; }
        public string ApplicationUserID { get; set; }
        public DateTime? Created_At { get; set; }
        public double Balance { get; set; }
        public double DisCount { get; set; }
        public int ParentId { get; set; }
    }
    public class EditUserViewModel
    {
        public string Id { get; set; }
        public IEnumerable<SelectListItem> RolesList { get; set; }
    }
}