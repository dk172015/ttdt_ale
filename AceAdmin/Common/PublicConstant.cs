﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AceAdmin.Common
{
    public class PublicConstant
    {
        public const string ROLE_ADMIN = "Admin";
        public const string ROLE_USERS = "User";

        //trạng thái
        public const int STATUS_PENDING = 0;
        public const int STATUS_ACTIVE = 1;
        public const int STATUS_DEACTIVE = 2;
        public const int STATUS_DELETE = 3;
        public const int STATUS_CHANGEPRICE = 4;

    }
}