﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AceAdmin.Startup))]
namespace AceAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
